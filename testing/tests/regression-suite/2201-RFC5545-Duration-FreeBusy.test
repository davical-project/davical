#
# Request a freebusy report by URL
#
# The event that we care about has DTSTART of 20210918T133000Z.
# And zero length events don't appear in Free/Busy, therefore this
# event will appear in the SQL result, but not the freebusy.php result.
#
TYPE=GET
HEAD

AUTH=user1:user1

REPLACE=/^DTSTAMP:\d{8}T\d{6}Z\r?$/DTSTAMP:yyyymmddThhmmssZ/
REPLACE=/^DTSTART:20210918T000000Z\r?$/DTSTART:correct/
REPLACE=/^DTEND:20210921T000000Z\r?$/DTEND:correct/

URL=http://regression.host/freebusy.php/user1/home/?start=20210918T000000&period=P3D

QUERY
SELECT calendar_item.rrule, calendar_item.status,
       dtstart AT TIME ZONE olson_name AS "a)     Start",
       olson_name AS "b)  Location",
       to_char(calendar_item.dtstart at time zone 'GMT','YYYYMMDD"T"HH24MISS"Z"') AS "c) UTC Start",
       to_char(calendar_item.dtend at time zone 'GMT','YYYYMMDD"T"HH24MISS"Z"') AS "d)   UTC End"
 FROM usr INNER JOIN collection USING (user_no)
          INNER JOIN caldav_data USING (collection_id)
          INNER JOIN calendar_item USING(dav_id)
          LEFT JOIN timezones ON (tz_id=tzid)
WHERE caldav_data.dav_name ~ '^/user1/home/'
  AND collection.is_calendar AND collection.schedule_transp = 'opaque'
  AND rrule_event_overlaps( dtstart, dtend, rrule, '20210918T000000Z', '20210921T000000Z' )
  AND caldav_data.caldav_type IN ( 'VEVENT', 'VFREEBUSY' )
  AND (calendar_item.transp != 'TRANSPARENT' OR calendar_item.transp IS NULL)
  AND (calendar_item.status != 'CANCELLED' OR calendar_item.status IS NULL)
 ORDER BY 3
ENDQUERY
