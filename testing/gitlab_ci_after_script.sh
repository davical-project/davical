#!/bin/bash

# Order: commandline, $CI_JOB_NAME, unknown
test="${1:-$CI_JOB_NAME}"
test="${test:-unknown}"

if [ -d /var/log/apache2 ]; then
    mkdir apache2_log
    cp -r /var/log/apache2 "apache2_log/$test"
    xz apache2_log/"$test"/*
fi

if [ -d /var/log/davical ]; then
    mkdir -p davical_log
    cp -r /var/log/davical "davical_log/$test"
fi

mkdir -p davical_conf/$test
cp /etc/davical/* davical_conf/$test
