-- Add CalDAV labels to principal types to remove hardcoded ID -> Label
-- mappings in code.
--
-- List is from: https://tools.ietf.org/html/rfc5545#section-3.2.3

BEGIN;
SELECT check_db_revision(1,3,5);

INSERT INTO roles ( role_no, role_name ) VALUES( 5, 'Room');

ALTER TABLE principal_type ADD COLUMN label VARCHAR;

-- Add labels
UPDATE principal_type
  SET label = 'INDIVIDUAL'
  WHERE principal_type_id = 1;

UPDATE principal_type
  SET label = 'RESOURCE'
  WHERE principal_type_id = 2;

UPDATE principal_type
  SET label = 'GROUP'
  WHERE principal_type_id = 3;

-- Add suport for the ROOM principal type.
-- Having hardcoded IDs pains me. A lot.
INSERT INTO principal_type
  (principal_type_id, principal_type_desc, label)
VALUES (4, 'Room', 'ROOM');

-- Ensure that the sequence is primed, on my instance the sequence had
-- last_value as 0.
SELECT setval('principal_type_principal_type_id_seq',
    (SELECT max(principal_type_id) FROM principal_type)
  );

-- http://blogs.transparent.com/polish/names-of-the-months-and-their-meaning/
SELECT new_db_revision(1,3,6, 'Czerwiec');

COMMIT;
ROLLBACK;
